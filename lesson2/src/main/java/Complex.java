public class Complex {
// Оставил две пустых строки

    private final double    FIRST; //Переименовал имя
    private final double    SECOND; //Переименовал имя


    public Complex(double FIRST, double SECOND) {
        if ((Double.isNaN(FIRST)) || (Double.isNaN(SECOND))) {     //Поставил скобки
            throw new ArithmeticException();
        }
        this.FIRST = FIRST;
        this.SECOND = SECOND;
    }


    public double realPart()      { return FIRST; }
    public double imaginaryPart() { return SECOND; }


    public Complex add(Complex c) {
        return new Complex(FIRST + c.FIRST, SECOND + c.SECOND);
    }


    @Override public boolean equals(Object o) {
        if (o == this) {     //Фигурные скобки в "if"
            return true;
        }
        if (!(o instanceof Complex)) {   //Фигурные скобки в "if"
            return false;
        }
        Complex c = (Complex) o;
        return (Double.compare(FIRST, c.FIRST) == 0) &&    //Поставил скобки
                (Double.compare(SECOND, c.SECOND) == 0);
    }


    @Override public int    hashCode() {
        int result = 17 + Double.hashCode(FIRST);
        result = 31 * result + Double.hashCode(SECOND);
        return result;
    }


    @Override public String toString() {
        return "(" + FIRST                 //Перенос на другую строку(для читабельности)
                + (SECOND < 0 ? "" : "+")
                + SECOND + "i)";
    }


    public static void main(String[] args) {
        Complex complex = new Complex(1, 2); //"z" непонятное имя
        System.out.println(complex.add(complex));
    }
}


/*
package ua.solution.narozhnyy;

public class Complex {
	private final double re;
    private final double im;

    public Complex(double re, double im) {
        if (Double.isNaN(re) || Double.isNaN(im)) {
            throw new ArithmeticException();
        }
        this.re = re;
        this.im = im;
    }

    public double realPart()      { return re; }
    public double imaginaryPart() { return im; }

    public Complex add(Complex c) {
        return new Complex(re + c.re, im + c.im);
    }

    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Complex))
            return false;
        Complex c = (Complex) o;
        return Double.compare(re, c.re) == 0 &&
               Double.compare(im, c.im) == 0;
    }

    @Override public int hashCode() {
        int result = 17 + Double.hashCode(re);
        result = 31 * result + Double.hashCode(im);
        return result;
    }

    @Override public String toString() {
        return "(" + re + (im < 0 ? "" : "+") + im + "i)";
    }

    public static void main(String[] args) {
        Complex z = new Complex(1, 2);
        System.out.println(z.add(z));
    }
}
 */